---
title: About
theme: responsive
video_player: true
---

# About Us

<div class="col-60", style="padding-right:20px;">

Code.org® er ein ideell organisasjon for å utvide tilgang til programmering i skoler og øke deltaking av kvinner og underrepresenterte minoritetar. Vår visjon er at kvar elev i kvar skule har høve til å lære datavitskap, på lik linje som biologi, kjemi eller algebra. We provide the most broadly used curriculum for teaching computer science in primary and secondary school and also organize the annual <a href="https://hourofcode.com">Hour of Code</a> campaign, which has engaged 10% of all students in the world. Code.org blir støtta av sjenerøse donasjonar frå Amazon, Facebook, Google, Infosys Foundation, Microsoft og <a href="/about/donors">mange fleire</a>.

</div>

[col-40]

<div style="background-color:#7665a0;height:190px;padding:25px;display:flex;justify-content:center;flex-direction:column">
  
  <font size="4" color="#FFFFFF"><b>Code.org courses are used by tens of millions of students and by one million teachers all around the world.</b></font>
 
</div>

[/col-40]

<div style="clear:both"></div>

## History 

In 2013, Code.org was launched by twin brothers Hadi and Ali Partovi with a [video promoting computer science](https://www.youtube.com/watch?v=nKIu9yen5nc). Denne videoen vart #1 på YouTube for ein dag og 15.000 skular tok kontakt med oss for å få hjelp. Sidan då har vi vokst frå frivillige til å byggje ein stor organisasjon som støtter ei verdsomfattande rørsle. Vi trur at ei god utdanning i datavitskap skal vere tilgjengeleg for kvart barn, ikkje berre nokre heldige få.
<br>
<iframe width="560" height="315" src="https://www.youtube.com/embed/nKIu9yen5nc" frameborder="0" allowfullscreen></iframe>

## What We Do

We do work across the education spectrum: designing [our own courses](https://studio.code.org/courses) or partnering with others, training teachers, partnering with large school districts, helping change government policies, expanding internationally via partnerships, and marketing to break stereotypes. Arbeidet byggjer på tiår med arbeid, utallige organisasjonar og enkeltpersonar som har bidratt til å etablere finansiere og spreie datautdanning. Vi er takksame for arbeidet som er gjort av det vidare datavitskap-nettverket og vi takkar alle partnarar og individer som har utvida vår påverknad opp gjennom åra.

## Code.org's International Reach 

Over 40 % av netttrafikken vår kjem frå land utanfor Usa og det talet held fram med å klatre. In order to expand global access to computer science, our team works closely with [more than 100 international partners](/about/partners), helping them to promote the Hour of Code, advocate for policy change, and train teachers. Vi gjer datavitskap del av internasjonal diskurs om utdanning ved å inngå partnerskap med utdanningsmyndigheiter over heile verda og ved å jobbe med internasjonale organisasjonar som Organisasjonen for økonomisk samarbeid og utvikling og Fns organisasjon for utdanning, vitenskap, kultur og kommunikasjon.

<img alt="Code.org International Partners" src="/images/international/international_partners.jpg" width="100%">

## Translation 

Ønsker du å direkte påverke elevar i landet ditt? Hjelp oss med å gjere innhaldet på Code.org tilgjengeleg på morsmålet ditt! Dersom du blir frivillig og hjelper med å oversette våre øvingar og oppgåver, vil du bli med i eit nettverk på meir enn 7 000 oversettarar som hjelper til med å få datautdanning til elevar over heile verda. Take a look at our [translation guide](/translate) for more information. 

## Our commitment to free curriculum and open source technology

All curriculum resources and tutorials we create will forever be free to use and openly licensed under a [Creative Commons](http://creativecommons.org/licenses/by-nc-sa/4.0/) license, allowing others to make derivative education resources for non-commercial purposes. If you are interested in licensing our materials for commercial purposes, [contact us](/contact). Våre kurs blir oversatt for globalt bruk eller av dei som snakker ulike språk. Our technology is developed as an [open source project](https://github.com/code-dot-org/code-dot-org).

<img alt="Students with a laptop" src="/images/international/group_computer.jpg" width="100%">

## Follow us

[Sign up to receive status updates](/about/hear-from-us) about progress in the K-12 computer science movement and about the work of Code.org. Or follow Code.org on social media:

{{ social_media_codeorg }}
