---
title: About
theme: responsive
video_player: true
---

# About Us

<div class="col-60", style="padding-right:20px;">

Code.org® ist eine gemeinnützige Organisation, die den Zugang zur Informatik in Schulen ausweiten und die Teilnahme von Frauen und anderen Minoritäten fördern will. Unsere Vision ist, dass jeder Schüler in jeder Schule die Möglichkeit hat, Informatik zu lernen, genauso wie Biologie, Chemie oder Algebra. We provide the most broadly used curriculum for teaching computer science in primary and secondary school and also organize the annual <a href="https://hourofcode.com">Hour of Code</a> campaign, which has engaged 10% of all students in the world. Code.org wird von großzügigen Spendern unterstützt, darunter Amazon, Facebook, Google, die Infosys Foundation, Microsoft und <a href="/about/donors">vielen weiteren</a>.

</div>

[col-40]

<div style="background-color:#7665a0;height:190px;padding:25px;display:flex;justify-content:center;flex-direction:column">
  
  <font size="4" color="#FFFFFF"><b>Code.org courses are used by tens of millions of students and by one million teachers all around the world.</b></font>
 
</div>

[/col-40]

<div style="clear:both"></div>

## History 

In 2013, Code.org was launched by twin brothers Hadi and Ali Partovi with a [video promoting computer science](https://www.youtube.com/watch?v=nKIu9yen5nc). Dieses Video wurde #1 auf YouTube für einen Tag, und 15.000 Schulen kontaktierten uns mit dem Wunsch nach Unterstützung. Seitdem sind wir von einer kleinen Gruppe Freiwilliger zu einer vollständigen Organisation gewachsen, die unsere weltweite Initiative durchführt. Wir sind der Meinung, dass für jedes Kind eine qualitativ hochwertige Informatik-Ausbildung zur Verfügung stehen sollte, nicht nur für ein paar glückliche.
<br>
<iframe width="560" height="315" src="https://www.youtube.com/embed/nKIu9yen5nc" frameborder="0" allowfullscreen></iframe>

## What We Do

We do work across the education spectrum: designing [our own courses](https://studio.code.org/courses) or partnering with others, training teachers, partnering with large school districts, helping change government policies, expanding internationally via partnerships, and marketing to break stereotypes. Unsere Arbeit baut auf jahrzehntelangen Bemühungen von unzähligen Organisationen und Einzelpersonen auf, die zur Etablierung, Finanzierung und Verbreitung der Informatik beigetragen haben. Wir sind dankbar für die unermüdliche Arbeit der breiten Informatik-Bildungsgemeinschaft, und wir danken allen Partnern und Einzelpersonen, die unsere Wirkung über die Jahre gestärkt haben.

## Code.org's International Reach 

Mehr als 40% unseres Websiteverkehrs kommen von außerhalb der Vereinigten Staaten und diese Zahl steigt weiter. In order to expand global access to computer science, our team works closely with [more than 100 international partners](/about/partners), helping them to promote the Hour of Code, advocate for policy change, and train teachers. Wir machen Informatik zu einem Teil des internationalen Bildungsdiskurses, indem wir mit Bildungsministerien aus aller Welt und mit internationalen Organisationen wie der Organisation für wirtschaftliche Zusammenarbeit und Entwicklung (OECD) und der Bildungs-, Wissenschafts- und Kulturorganisation der Vereinten Nationen (UN) zusammenarbeiten.

<img alt="Code.org International Partners" src="/images/international/international_partners.jpg" width="100%">

## Translation 

Möchtest Du direkten Einfluss auf Schüler in Deinem Land haben? Hilf uns, Code.org-Inhalte in Deiner Muttersprache verfügbar zu machen! Wenn Du uns bei der Übersetzung unserer Tutorials und Lektionen mithilfst, wirst Du Teil einer Gemeinschaft von mehr als 7.000 Übersetzern, die helfen, Informatik-Bildung zu Schülern in der ganzen Welt zu transportieren. Take a look at our [translation guide](/translate) for more information. 

## Our commitment to free curriculum and open source technology

All curriculum resources and tutorials we create will forever be free to use and openly licensed under a [Creative Commons](http://creativecommons.org/licenses/by-nc-sa/4.0/) license, allowing others to make derivative education resources for non-commercial purposes. If you are interested in licensing our materials for commercial purposes, [contact us](/contact). Unsere Kurse werden für den weltweiten Gebrauch oder für Vorträge in verschiedene Sprachen übersetzt. Our technology is developed as an [open source project](https://github.com/code-dot-org/code-dot-org).

<img alt="Students with a laptop" src="/images/international/group_computer.jpg" width="100%">

## Follow us

[Sign up to receive status updates](/about/hear-from-us) about progress in the K-12 computer science movement and about the work of Code.org. Or follow Code.org on social media:

{{ social_media_codeorg }}
