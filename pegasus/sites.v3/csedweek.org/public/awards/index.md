---
title: 2019 Champions of Computer Science Awards
nav: about_nav
theme: responsive
---

<a id="top"></a>

# 2019 Champions of Computer Science Awards

In celebration of the 2019 [Computer Science Education Week](https://csedweek.org/) (CSEdWeek), the [Computer Science Teachers Association](http://www.csteachers.org/) (CSTA) and [Code.org](https://code.org/) are pleased to announce that submissions for the third annual Champions of Computer Science AWards are now open! 
<br>
<br>
The purpose of the Champions of CS Awards is to identify and celebrate students, teachers, administrators, and organizations who have made a significant impact to improve access to and the quality of CS education.
<br>
<br>
Any K-12 student, teacher, or administrator may be nominated. Organizations that work directly to improve access to and the quality of CS education may also be nominated (examples: afterschool coding clubs, local CSTA chapters, regional or national nonprofits, etc.). This opportunity is only available for US submissions.
<br>
<br>
Award winners and a guest of choice will receive an all-expenses paid trip (travel & hotel) to attend the CSEdWeek event on December 9th (location to be confirmed).
<br>
<br>
Submit your nominations using this form. The deadline for submission is Monday, October 21, 2019 at midnight PT. Email awards@csteachers.org if you have questions. You can take a look at last year’s winners below!
<br>
<br>

# 2018 Winners

[col-25]

<a href="/awards/2018/jocelyn-marencik">
    <center><img src="/images/awards/2018/fit-540/jocelyn-marencik-1.jpg" width="90%"></center>
    <div style="margin-top:5px;font-size:16px;text-align:center">Jocelyn Marencik</div>
</a>

[/col-25]

[col-25]

<a href="/awards/2018/robert-defillippo">
    <center><img src="/images/awards/2018/fit-540/defillippo-1.jpg" width="90%"></center>
    <div style="margin-top:5px;font-size:16px;text-align:center">Robert DeFillippo</div>
</a>

[/col-25]

[col-25]

<a href="/awards/2018/chanel-white">
    <center><img src="/images/awards/2018/fit-540/chanel-white-2.jpg" width="90%"></center>
    <div style="margin-top:5px;font-size:16px;text-align:center">Chanel White</div>
</a>

[/col-25]

[col-25]

<a href="/awards/2018/seasidehs">
    <center><img src="/images/awards/2018/fit-540/seasidehs.jpg" width="90%"></center>
    <div style="margin-top:5px;font-size:16px;text-align:center">Seaside High School</div>
</a>

[/col-25]

<div style="clear:both"></div>

<br>
<br>

[col-25]

<a href="/awards/2018/lps">
    <center><img src="/images/awards/2018/fit-540/lps-1.jpg" width="90%"></center>
    <div style="margin-top:5px;font-size:16px;text-align:center">Lincoln Public Schools</div>
</a>

[/col-25]

[col-25]

<a href="/awards/2018/gcwg">
    <center><img src="/images/awards/2018/fit-540/gcwg-profile.jpg" width="90%"></center>
    <div style="margin-top:5px;font-size:16px;text-align:center">GirlsCodingWithGirls</div>
</a>

[/col-25]

[col-25]

<a href="/awards/2018/richard-ladner">
    <center><img src="/images/awards/2018/fit-540/richard-ladner-1.jpg" width="90%"></center>
    <div style="margin-top:5px;font-size:16px;text-align:center">Richard Ladner</div>
</a>

[/col-25]

[col-25]

<a href="/awards/2018/quorum">
    <center><img src="/images/awards/2018/fit-540/quorum-profile.png" width="90%"></center>
    <div style="margin-top:5px;font-size:16px;text-align:center">The Quorum Programming Team</div>
</a>

[/col-25]

<div style="clear:both"></div>

# 2017 Winners

## Students

<a href="/awards/2017/reynaga_garcia">
    <div style="margin-top:5px;font-size:16px;text-align:left">Angela Garcia Pena and Crystal Reynaga</div>
</a>

<a href="/awards/2017/branum">
    <div style="margin-top:5px;font-size:16px;text-align:left">Mason Branum</div>
</a>

<a href="/awards/2017/shekhar">
    <div style="margin-top:5px;font-size:16px;text-align:left">Shreya Shekhar</div>
</a>

<a href="/awards/2017/srivastava">
    <div style="margin-top:5px;font-size:16px;text-align:left">Vidhi Srivastava</div>
</a>

## Teachers

<a href="/awards/2017/neville">
    <div style="margin-top:5px;font-size:16px;text-align:left">Diane Neville</div>
</a>

<a href="/awards/2017/lopez">
    <div style="margin-top:5px;font-size:16px;text-align:left">Efraín López</div>
</a>

<a href="/awards/2017/sutkowski">
    <div style="margin-top:5px;font-size:16px;text-align:left">Heather Sutkowski</div>
</a>

<a href="/awards/2017/ramos">
    <div style="margin-top:5px;font-size:16px;text-align:left">Lawrence Ramos</div>
</a>

## Schools

<a href="/awards/2017/ams">
    <div style="margin-top:5px;font-size:16px;text-align:left">Anacapa Middle School</div>
</a>

<a href="/awards/2017/asmsa">
    <div style="margin-top:5px;font-size:16px;text-align:left">Arkansas School for Mathematics, Sciences and the Arts</div>
</a>

<a href="/awards/2017/bhs">
    <div style="margin-top:5px;font-size:16px;text-align:left">Brenham High School</div>
</a>

<a href="/awards/2017/ehs">
    <div style="margin-top:5px;font-size:16px;text-align:left">Eufaula High School</div>
</a>

## District

<a href="/awards/2017/sfusd">
    <div style="margin-top:5px;font-size:16px;text-align:left">San Francisco Unified School District</div>
</a>

## Organizations

<a href="/awards/2017/qcra">
    <div style="margin-top:5px;font-size:16px;text-align:left">Queen City Robotics Alliance</div>
</a>

<a href="/awards/2017/sbcs">
    <div style="margin-top:5px;font-size:16px;text-align:left">South Bend Code School</div>
</a>

<div style="clear:both"></div>

### []()
### []()

