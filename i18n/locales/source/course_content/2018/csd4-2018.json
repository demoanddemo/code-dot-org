{
  "https://studio.code.org/s/csd4-2018/stage/1/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/2/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/3/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/4/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/5/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/6/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/7/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/8/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/9/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/10/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/10/puzzle/2": {
    "dsls": {
      "markdown": "# Example App Types\n\nIn the following levels you see several different example apps. They will give you an idea of what the UI (User Interface) could look like for apps with different goals.\n\nFor each example app:\n\n* Click through the example to get a feel for how it functions.\n* In your activity guide, make a note of any features or UI elements you find and what they are used for.\n* Discuss with your groups how that app and any of its UI features could be applied to your chosen topic.\n\n# A Note About Functionality\n\nThese apps are _prototypes_; they allow a user to interact with the design without worrying about the underlying functionality. For example, when you take use the Quiz App, each question will always respond the same way, regardless of which answer you choose.\n\nBy focusing on an interactive prototype first you can learn a lot about how users might use your app before investing too much energy in making it actually work.\n"
    }
  },
  "https://studio.code.org/s/csd4-2018/stage/10/puzzle/3": {
    "long_instructions": "# Quiz Me\n\nA quiz app can be used to teach, or help reinforce, information important to your chosen topic.\n\nAt its most basic, a quiz app is composed of a screen for each question with two or more buttons for multiple choice answers. Each button links to a page that either confirms that you were correct, or gives you information about why you were were wrong.\n\nMore advanced quiz apps could include features such as:\n\n-   Different types of questions (such as matching, free response, or even interactive games)\n-   Randomizing the order of questions\n-   Changing the questions based on how we'll you're doing\n-   Providing a final score\n-   Incorporating study material\n\n# Functionality Missing from this Prototype\n\n-   Questions aren't actually checked for correctness"
  },
  "https://studio.code.org/s/csd4-2018/stage/10/puzzle/4": {
    "long_instructions": "# Decision Maker\n\nSimilar to the quiz app, a decision tree app asks the user a series of questions. The primary difference between the two is that a decision tree doesn't check for right or wrong answers, but instead attempts to give the user a suggestion or help them make a decision based on their answers.\n\nThis simple decision maker asks only three questions,  but more advanced ones actually take the form of a tree as seen below:\n\n[][0]\n\nDepending on the user's answer to each question, they're given a different page for the next question. This allows the app to gradually narrow down the decision one question at a time. This type of app can be really useful to help users understand complex processes - for example, you might introduce a recycling program by using a decision tree app to help users figure out which bin to sort items into - first asking what material it's made out of, then whether it is clean or not, and so on.\n\n# Functionality Missing from this Prototype\n\n-   Question buttons don't direct you to different screens\n-   The final screen doesn't give you a recommended decision"
  },
  "https://studio.code.org/s/csd4-2018/stage/10/puzzle/5": {
    "long_instructions": "# List Manager\n\nThe list manager app allows user to keep track of the state of information and can be modified to serve many different purposes, such as checking in / out loaned items, tracking personal To Dos, or keeping track of where things are in a multi-step process.\n\nWhile this example focuses on a single list, more sophisticated list apps could allow the user to create multiple lists, or even move items between lists. This app also only keeps track of the name of an item and whether or not it is \"Done,\" but it could be extended to track and organize any amount of information about the list items.\n\n# Functionality Missing from this Prototype\n\n-   Delete buttons don't actually delete items from the list\n-   Add screen doesn't actually add an item to the list"
  },
  "https://studio.code.org/s/csd4-2018/stage/10/puzzle/6": {
    "long_instructions": "# Crowdsourcing\n\nCrowdsourcing apps allow multiple users to submit content for everyone to use (the _source_ of content is the _crowd_). This app is very similar to the list app, with the main difference being that the list app assumes a single user while this app allows all users to add to the same list and \"like\" submissions.\n\nThis example uses a list of crowdsourced information for simplicity, but you could display any kind of information in many different formats. A crowdsourcing app could allow users to submit locations for display on a map, images to go into a gallery, or any number of other types of information.\n\n# Functionality Missing from this Prototype\n\n-   User's can't create accounts or profiles\n-   Submitted things aren't added to the list\n-   Clicking on the \"like\" icons don't change their state"
  },
  "https://studio.code.org/s/csd4-2018/stage/11/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/2": {
    "display_name": "Tour of Design Mode"
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/3": {
    "display_name": "Intro to Design Mode"
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/4": {
    "short_instructions": "Add a large red button to your screen using Design Mode. (Click for full instructions.)",
    "long_instructions": "<div markdown=\"1\" style=\"float: right; margin: 20px;\">\n\n[expandable][0]\n\n<br/>\n\nClick to expand\n\n</div>\n\n# Creating a digital prototype\n\nUsing the prototype to the right (click the image to enlarge it) you're going to create a digital prototype of the example app \"Recycling Finder\". We've already added a few of the elements for you, including the title, but it doesn't look quite right yet. We'll go over each element of this as we go, but for now let's **just focus on the title**.\n\n<!--<img src=\"https://images.code.org/66633526e03550bedb8503fefdc2ece3-image-1502921575904.png\" style=\"width: 300px; float: right; clear: both;\">-->\n\n# Do This\n\n-   **Make sure you are in Design Mode,** the switch appears above your app window on the left.\n-   **Click on the title text** to view its properties.\n-   **Change the property \"text\"** to \"Recycling Finder\".\n-   **Change the \"font size\"** to make the title larger.\n-   **Change the \"text alignment\"** so the title is centered on the screen.\n\n_Note: Make sure you click **\"Run\"** before moving on to save your progress._"
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/5": {
    "long_instructions": "<div markdown=\"1\" style=\"float: right; margin: 20px;\">\n\n[expandable][0]\n\n<br/>\n\nClick to expand\n\n</div>\n\n# Adding more text\n\nEach screen of your app is composed of multiple design _elements_. You can drag out elements from the \"Design Toolbox\" on the left side of the workspace. To add more blocks of text, just drag out additional \"label\" elements.\n\n# Do This\n\n-   **Drag out a new \"label\" element** and place it where the \"home\\_description\" text should be.\n-   **Change the id** to match the prototype (home\\_description)\n-   **Change the field \"text\"** to match the prototype.\n-   **Resize** the text area by clicking and dragging the bottom right corner.\n-   **Change any other properties** to make the text look like you want it to."
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/6": {
    "long_instructions": "<div markdown=\"1\" style=\"float: right; margin: 20px;\">\n\n[expandable][0]\n\n<br/>\n\nClick to expand\n\n</div>\n\n# Adding images\n\nThe prototype just has a plain black square where the logo should be - let's add an image to spice it up. The \"image\" element allows you to either upload an image from your computer or select from a library of graphic icons.\n\n# Do This\n\n-   **Drag out a new \"image\" element** and place between the description and the title.\n-   **Change the id** to match the prototype.\n-   **Click the \"Choose...\" link** next to the \"image\" field.\n-   **Select the \"Icons\" tab** and choose an icon for your logo.\n-   **Change the color** of the icon using the \"icon color\" field."
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/7": {
    "long_instructions": "<div markdown=\"1\" style=\"float: right; margin: 20px;\">\n\n[expandable][0]\n\n<br/>\n\nClick to expand\n\n</div>\n\n# Adding buttons\n\nButtons allow your user to interact with your app. This prototype screen will need three buttons (each shaded gray on the prototype.) We've already added the Search button, so you just need to add Contact and About.\n\n# Do This\n\n-   **Drag out a new \"button\" element** for each of the two remaining buttons.\n-   **Change the ids** to match the prototype.\n-   **Change the text** to match the prototype.\n-   **Modify other properties** to make the screen look like you'd like.\n\n<img src=\"https://images.code.org/4cab6a5233cf07df0d8e312932762012-image-1444314797615.gif\" style=\"width: 500px;\">"
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/8": {
    "display_name": "Design Mode Elements"
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/9": {
    "long_instructions": "<div markdown=\"1\" style=\"float: right; margin: 20px;\">\n\n[expandable][0]\n\n<br/>\n\nClick to expand\n\n</div>\n\n# Making a Search Box\n\nThe search box in our prototype can be created using a \"Text Input\" element. This is a box that your user can type into, and later your software can read what was entered and do something with it.\n\n# Do This\n\n-   **Add a Text Input** element.\n-   **Change the id** to match the prototype.\n-   **Add a Label** with the text \"Location\" next to your text input."
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/10": {
    "long_instructions": "<div markdown=\"1\" style=\"float: right; margin: 20px;\">\n\n[expandable][0]\n\n<br/>\n\nClick to expand\n\n</div>\n\n# Checkboxes\n\nThe type of recyclables you're searching for can be set using \"Checkbox\" elements. We already added a checkbox and label for glass, so you just need to add ones for paper and plastic.\n\n# Do This\n\n-   **Add a Checkbox** for each of the options.\n-   **Change the ids** to match the prototype.\n-   **Add labels** to describe each checkbox."
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/11": {
    "long_instructions": "<div markdown=\"1\" style=\"float: right; margin: 20px;\">\n\n[expandable][0]\n\n<br/>\n\nClick to expand\n\n</div>\n\n# Finishing Touches\n\nBy now you should have a fairly complete (though non-functional) digital version of the paper prototype we started with. All that's left to do is check back over your paper prototype and add any final touches you might have missed.\n\n# Do This\n\n-   **Check over the prototype** to make sure you've included everything.\n-   **Read through your IDs** and fix any that aren't descriptive.\n-   **Clean up** any rough edges by resizing or moving around elements."
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/12": {
    "dsls": {
      "markdown": "# Mocking Up Your App\n\nNow that you have some practice laying out elements in App Lab, you can start working on mocking your own app. With your group, divvy up the screens so that everyone is working on a different screen.\n\nBecause you'll be dividing the work up, you'll want to do some planning ahead of time to decide on a common look and feel for your pages. For example, you'll want to agree on things such as:\n\n* Where do navigation buttons go?\n* What colors will you use for backgrounds, text, and buttons\n* What's the general style and layout?\n\n# Namespacing\n\nIn the next lesson you will combine all of your team members' screens into one app. To make sure that the IDs on one page don't conflict with another you'll need to include a unique _namespace_ for your page. This will be something you add to the beginning of every id so that your element ids don't conflict with others when you merge everyone's screens together. Your namespace should be the ID of your screen with an underscore at the end. For example, if you are making the home page, you might use \"home_\" as your namespace - leading to ids such as:\n\n* \"home_title\"\n* \"home_loginButton\"\n* \"home_logo\"\n"
    }
  },
  "https://studio.code.org/s/csd4-2018/stage/12/puzzle/13": {
    "display_name": "App Project: Screen Design",
    "long_instructions": "# Mocking Up Your App\n\nUsing your paper prototype, create a digital version of your screen using the Design Mode elements you've learned about.\n\n# Do This\n\n-   Rename the screen to match your _namespace_.\n-   Use the built-in elements to lay out your app screen, giving each a proper ID\n    -   For example, if your namespace is `home_` you might create IDs like:\n        -   `home_title`\n        -   `home_login_button`\n        -   `home_logo`\n-   For any components of your app that can't be recreated with the built in elements you can either:\n    -   Redesign to utilize built in elements\n    -   Find an image to use in place of your element\n    -   Draw the element using your preferred image editor\n-   Make sure that you and your teammates are regularly looking at each other's designs to ensure consistent style"
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/2": {
    "display_name": "Multi-Screen Apps"
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/3": {
    "long_instructions": "# Adding a New Screen\n\nTo complete your app, you're going to need more than just a home screen. Each card that you created in your paper prototype is represented by a \"screen\" in App Lab. To create more screens, just click the dropdown at the top of the app display and select \"New Screen...\"\n\n<img src=\"https://images.code.org/2b49c36a57fb904d4fcf219ca498647d-image-1504714832027.png\" style=\"width: 200px\">\n\n# Do This\n\n-   **Create a New Screen** and give it the ID \"about\"."
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/4": {
    "long_instructions": "<div markdown=\"1\" style=\"float: right; margin: 20px;\">\n\n[expandable][0]\n\n<br/>\n\nClick to expand\n\n</div>\n\n# Design the About Screen\n\nTo add elements to your new About screen, you'll need to first select it from the screens dropdown.\n\n# Do This\n\n-   **Complete the About screen** using the prototype to the right.\n-   **Use the screens dropdown** to switch between your Home and About screens."
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/5": {
    "display_name": "Importing Screens"
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/6": {
    "long_instructions": "<div markdown=\"1\" style=\"float: right; margin: 20px;\">\n\n[expandable][0]\n\n<br/>\n\nClick to expand\n\n</div>\n\n# Screen Import\n\nIn addition to adding new blank screens, you can import screens from \"Import Screen...\" To import a screen from someone else, you'll need them to give you the sharing URL.\n\n**Note:** Screens you import _must not_ share any IDs with elements already in your app!\n\n# Do This\n\nAnother student has created a search results screen that you can import into your app. Their app's share link is **<a href=\"https://studio.code.org/projects/applab/XkcpDVj8MJWQvUr9OSgIlA/\" target=\"_blank\">https\\://studio.code.org/projects/applab/XkcpDVj8MJWQvUr9OSgIlA/</a>**\n\n-   **Select \"Import Screen...** from the screens dropdown.\n-   **Copy and paste the above url** into the import screens dialog.\n-   **Select the screen** you wish to import.\n-   **Click Import** to import the screen.\n-   **Use the screens dropdown** to switch between your Home, About, and Search screens."
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/7": {
    "long_instructions": "# Contact Screen Import\n\nYet another student created a Contact screen that you can import into your app. Their share link is **<a href =\"https://studio.code.org/projects/applab/QUAOln68kifScEwQauwNqw/\" target=\"_blank\">https\\://studio.code.org/projects/applab/QUAOln68kifScEwQauwNqw/</a>**\n\n# Do This\n\n-   **Select \"Import Screen...** from the screens dropdown.\n-   **Copy and paste the above url** into the import screens dialog.\n-   **Select the screen** you wish to import.\n-   **Click Import** to import the screen.\n-   **Use the screens dropdown** to make sure your app has Home, About, Contact, and Search screens."
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/8": {
    "display_name": "Responding to User Input"
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/9": {
    "long_instructions": "# Button Events\n\nWith our screens in place, it's time to start actually _programming_ the app so that it responds to button clicks. For each button on each screen, you'll want to add an `onEvent` block that watches for that button to be clicked and responds appropriately. To start off with we'll just watch the `home_search_btn` button and print something to the console when it's clicked\n\n# Do This\n\nYou're now in Code Mode (you can use the buttons above your app to switch between Code Mode and Design Mode). This is where you can write the code that will respond to users interacting with your app.\n\n[][0]\n\n-   **Drag out an `onEvent` block** from the code toolbox.\n-   **Select `home_search_btn`** from the \"id\" dropdown.\n-   **Drag out a `console.log` block** from the variables drawer.\n-   **Run your app**.\n-   **Click the Search button** and look for messages in the console."
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/10": {
    "display_name": "Changing Screens"
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/11": {
    "long_instructions": "# Changing Screens On Click\n\nThe `setScreen` block will change your app to whichever screen you specify. If you place a `setScreen` block _inside_ an `onEvent` block, you can program your buttons to change to the correct screen.\n\n# Do This\n\nUsing the `onEvent` block that you've created, make your program change to the \"search\" screen when the `home_search_btn` is clicked.\n\n-   **Drag out a `setScreen` block** an put it inside the `onEvent` block.\n-   **Select \"search\"** from the `setScreen` dropdown.\n-   **Run your app** and test the `home_search_btn` button."
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/12": {
    "long_instructions": "# Wire Up the Other Buttons\n\nNow that you've made one button work, just follow the same pattern for all of your others.\n\n# Do This\n\n-   **Add an `onEvent` block** for each button.\n-   **Select the button ID** from the \"id\" dropdown.\n-   **Add a `setScreen` block** inside each `onEvent` block.\n-   **Select the screen ID** from each `setScreen` dropdown.\n-   **Run your app** and test that all of the buttons work."
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/13": {
    "display_name": "App Project: Screen Import",
    "long_instructions": "# Back To Your App\n\nNow that you've had some practice with importing screens, it's time to start working on your team's app.\n\n# Do This\n\nEach member of your team is going to create their own copy of the app, with all of the team members' screens imported in. This will be your copy of the app, and the copy that you will start to add code to in the next lesson.  (Don't forget that you'll also need to import your own screen into this project.)\n\n-   Collect all of the sharing urls for your team's pages, **including your own**\n    -   Each team member can find the share url for their own screen by going back to the last level of the previous lesson, or by finding the screen in their [projects directory][0]\n-   Import each of the screens, one at a time\n    -   If you run into an ID error with one of the screens, discuss the issue with the screen's creator so it can be fixed and imported\n-   Delete the default \"screen1\"\n-   Set the main screen as the default\n\nDon't worry about adding any code at this point; we'll get to that in the next level."
  },
  "https://studio.code.org/s/csd4-2018/stage/13/puzzle/14": {
    "display_name": "App Project: Linking Screens",
    "long_instructions": "# Linking Screens\n\nWith all of your screens in place you can start adding events that will change the screens. When you're done with this step you'll actually have an interactive prototype!\n\n# Do This\n\nFor each screen of your app:\n\n-   Find all of the button IDs\n-   For each button, add an event handler that watches that ID\n-   In each event handler, use `setScreen()` to move the the right screen\n-   Test it all out!\n\nDepending on the number of screens and buttons your app has, this can be a pretty involved process. Make sure that you test your work often, using `console.log()` blocks to debug any strange behavior with your app. When you think you've got the whole thing working, compare your app with other members of your group to see if they work the same."
  },
  "https://studio.code.org/s/csd4-2018/stage/14/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/15/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/15/puzzle/2": {
    "display_name": "App Project: Bugs and Features",
    "long_instructions": "# Improving and Iterating\n\nYou've put your app in front of users, gathered feedback, and identified bugs and new features - time to do something about it.\n\n# Do This\n\nEach team member is going to implement select features and bug fixes on their own version of the team's app. Start by selecting a sticky note from the **To Do** column of your team's chart and moving it to the **Doing** column. For each sticky you tackle:\n\n-   Add a comment to the top of your program describing the feature or bug fix you are implementing\n-   Work on your iteration until it works. Try to stay focused on only the single feature or bug that you've selected.\n-   When done, move your sticky to the **Done** column, pick a new one, and start the process over."
  },
  "https://studio.code.org/s/csd4-2018/stage/16/puzzle/1": {
    "display_name": "Lesson Overview"
  },
  "https://studio.code.org/s/csd4-2018/stage/17/puzzle/1": {
    "dsls": {
      "markdown": "# CS Discoveries Post-Course Survey\n\n**Your input and feedback is important to us!** We use it to:\n\n* understand your experience\n* make improvements to the course\n\nThanks for taking the time to help make CS Discoveries even better!\n\n<a class=\"btn btn-large btn-success\" href=\"/s/csd-post-survey-2018/stage/1/puzzle/1/page/1\" target=\"top\">Click here to complete the CS Discoveries Post-Course Survey</a>\n\n(opens in a new tab)\n\n<br>\n\n\n\n\n\n"
    }
  }
}