login_required true
hideable_stages true
has_verified_resources true
has_lesson_plan true
curriculum_path 'https://curriculum.code.org/{LOCALE}/cspilot/unit3/{LESSON}'
pilot_experiment 'csp-piloters'
curriculum_umbrella 'CSP'

stage 'Introduction to Apps'
level 'CSP U3L1 SFLP 20-21', progression: 'Lesson Overview'
level 'CSP U3 Water Conservation Sample App', progression: 'App Exploration'
level 'CSP U3 Bird Quiz App', progression: 'App Exploration'
level 'CSP U3 Hamilton Township App', progression: 'App Exploration'
level 'CSP U3 Four Square App', progression: 'App Exploration'
level 'CSP U3 Monarch Butterfly App', progression: 'App Exploration'
level 'How Computers Work part 1 CSP', progression: 'How Computers Work'
level 'CSP U3 Water Conservation Sample App Investigate', progression: 'App Investigation'
level 'CSP U3 Bird Quiz App Investigation', progression: 'App Investigation'
level 'CSP U3 Hamilton Township App Investigation', progression: 'App Investigation'
level 'CSP U3 Four Square App Investigation', progression: 'App Investigation'
level 'CSP U3 Monarch Butterfly App Investigation', progression: 'App Investigation'
level 'CSP U3 L1 Input Output Matching', progression: 'Check For Understanding', assessment: true

stage 'Introduction to Design Mode'
level 'CSP U3L2 SFLP 20-21', progression: 'Lesson Overview'
level 'CSP U3 Hamilton Township Design Mode', progression: 'Design Mode Exploration'
level 'CSP U3 L2 Design Mode Themes', progression: 'Design Mode Exploration'
level 'CSP U3 L2 Design Mode Colors', progression: 'Design Mode Exploration'
level 'CSP U3 L2 Design Mode Images', progression: 'Design Mode Exploration'
level 'CSP U3 L2 Design Mode Icons', progression: 'Design Mode Exploration'
level 'CSP U3 L2 Design Mode ids', progression: 'Design Mode Exploration'
level 'CSP U3 L2 Copy an App', progression: 'Copy an App'
level 'U3L2 CSP CFU ID names', progression: 'Check For Understanding', assessment: true

stage 'Project - Designing an App Part 1'
level 'CSP U3L3 SFLP 20-21', progression: 'Lesson Overview'
level 'CSP U3 L3 Check Your Understanding', progression: 'Check For Understanding', assessment: true

stage 'Project - Designing an App Part 2'
level 'CSP U3L4 SFLP 20-21', progression: 'Lesson Overview'
level 'CSP U3 Bird Quiz Storyboard', progression: 'Sample Prototype'
level 'CSP U3 L4 Design Screens', progression: 'Build Your Screens'

stage 'Programs Explore'
level 'CSP U3L5 SFLP 20-21', progression: 'Lesson Overview'
level 'U3L5 CSP CFU natural language', progression: 'Check For Understanding', assessment: true

stage 'Programs Investigate'
level 'CSP U3L6 SFLP 20-21', progression: 'Lesson Overview'
level 'U3 Program Investigate Sequential 1', progression: 'Code Investigation'
level 'U3 Program Investigate Sequential 2', progression: 'Code Investigation'
level 'U3 Program Investigate Event-Driven 1', progression: 'Code Investigation'
level 'U3 Program Investigate Event-Driven 2', progression: 'Code Investigation'
level 'U3 Program Investigate Event-Driven 3', progression: 'Code Investigation'
level 'U3L6 CSP CFU Sequential vs Event Driven', progression: 'Check for Understanding', assessment: true

stage 'Programs Practice'
level 'CSP U3L7 SFLP 20-21', progression: 'Lesson Overview'
level 'U3 Program Practice Pt 1', progression: 'Debugging'
level 'U3 Program Practice Pt 2', progression: 'Debugging'
level 'U3 Program Practice Pt 3', progression: 'Debugging'
level 'U3 Program Practice Pt 4', progression: 'Debugging'
level 'U3 Program Practice Pt 5', progression: 'Debugging'
level 'U3 Program Practice Pt 6', progression: 'Debugging'
level 'U3 Program Practice Pt 8', progression: 'Debugging'
level 'U3L7 CSP CFU What was confusing', progression: 'Check for Understanding', assessment: true

stage 'Project - Designing an App Part 3'
level 'CSP U3L8 SFLP 20-21', progression: 'Lesson Overview'
level 'U3 L08 Pair Programming Video', progression: 'Pair Programming'
level 'CSP U3 L4 Add Code', progression: 'Add Code'

stage 'Project - Designing an App Part 4'
level 'CSP U3L9 SFLP 20-21', progression: 'Lesson Overview'
level 'CSP U3 L9 Add More Code', progression: 'Add More Code'

stage 'Project: Finish Your App'
level 'CSP U3L10 SFLP 20-21', progression: 'Lesson Overview'
level 'CSP U3 L10 Submit App', progression: 'Submit Your App', assessment: true

stage 'Lesson 11: Unit Assessment', lockable: true
level 'CSP Unit 3 MC Assessment _2020', assessment: true

stage 'Unit 3 STUDENT Survey', lockable: true, flex_category: 'required'
level 'CSP U3 20-21 Student Pilot Survey', progression: 'Unit 1 STUDENT Survey', assessment: true

stage 'Unit 3 TEACHER Survey', lockable: true, flex_category: 'required'
level 'CSP U3 20-21 Teacher Pilot Survey', progression: 'Unit 1 TEACHER Survey', assessment: true
