hidden false
hideable_stages true
teacher_resources [["lessonPlans", "https://curriculum.code.org/csf-19/coursea/"], ["teacherForum", "http://forum.code.org/c/csf"], ["vocabulary", "https://curriculum.code.org/csf-19/coursea/vocab/"], ["standardMappings", "https://curriculum.code.org/csf-19/coursea/standards/"]]
stage_extras_available true
has_lesson_plan true
curriculum_path 'https://curriculum.code.org/{LOCALE}/csf-19/coursea/{LESSON}'
project_widget_visible true
project_widget_types ["playlab_k1", "artist_k1"]
script_announcements [{"notice"=>"Our 2019 CS Fundamentals Lessons are Live!", "details"=>"Click \"Learn More\" to view all of the lesson plans for the 2019 version of Course A!", "link"=>"https://curriculum.code.org/csf-19/coursea", "type"=>"information", "visibility"=>"Teacher-only"}]
family_name 'coursea'
version_year '2019'
is_stable true
curriculum_umbrella 'CSF'

stage 'Going Places Safely', flex_category: 'csf_digcit'
level 'GoingPlacesSafely-unplugged'

stage 'Learn to Drag and Drop', flex_category: 'csf_sequencing'
skin 'jigsaw'
level 'blockly:Jigsaw:1', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:2', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:3', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:4', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:5', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:6', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:7', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:8', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:9', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:10', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:11', progression: 'Practice'
skin 'jigsaw'
level 'blockly:Jigsaw:12', progression: 'Practice'

stage 'Programming: Happy Maps', flex_category: 'csf_sequencing'
level 'HappyMaps-Unplugged'

stage 'Sequencing with Scrat', flex_category: 'csf_sequencing'
level 'courseAB_video_NewIntro_2019', progression: 'Maze Intro: Programming with Blocks'
level 'courseB_Scrat_ramp1_2019', progression: 'Practice'
level 'courseB_Scrat_ramp2_2019', progression: 'Practice'
level 'courseB_Scrat_ramp3a_2019', progression: 'Practice'
level 'courseB_Scrat_ramp3b_2019', progression: 'Practice'
level 'courseB_Scrat_ramp4a_2019', progression: 'Practice'
level 'courseB_Scrat_ramp5a_2019', progression: 'Practice'

stage 'Programming in Ice Age', flex_category: 'csf_sequencing'
level 'elementary_video_pairProgramming_2019', progression: 'Pair Programming'
level 'courseB_maze_seq1_2019', progression: 'Practice'
level 'courseB_maze_seq4_2019', progression: 'Practice'
level 'courseB_maze_seq5_2019', progression: 'Practice'
level 'courseB_maze_seq6_2019', progression: 'Practice'
level 'courseAB_video_debugging_2019', progression: 'Debugging with the Step Button'
level 'courseB_maze_seq7_2019', progression: 'Practice'
level 'courseB_maze_seq8_2019', progression: 'Practice'
level 'courseB_maze_seq9_2019', progression: 'Challenge', challenge: true
level 'courseB_maze_seq10_2019', progression: 'Practice'
level 'courseB_maze_seq11_2019', progression: 'Practice'
level 'courseB_maze_seq12_2019', progression: 'Practice'
bonus 'courseB_maze_seq_challenge1_2019'
bonus 'courseB_maze_seq_challenge2_2019'

stage 'Programming with Rey and BB-8', flex_category: 'csf_sequencing'
level 'bb8_skinOverview_K-1_video_2019', progression: 'Programming with Rey and BB-8'
level 'courseB_starWars_prog1_2019', progression: 'Practice'
level 'courseB_starWars_prog2_2019', progression: 'Practice'
level 'courseB_starWars_prog3_2019', progression: 'Practice'
level 'courseB_starWars_prog4_2019', progression: 'Practice'
level 'courseB_starWars_prog5_2019', progression: 'Practice'
level 'courseB_starWars_prog6_2019', progression: 'Practice'
level 'courseB_starWars_prog7_2019', progression: 'Practice'
level 'courseB_starWars_prog8_2019', progression: 'Challenge', challenge: true
level 'courseB_starWars_prog9_2019', progression: 'Practice'
level 'courseB_starWars_prog10_2019', progression: 'Practice'
level 'courseB_starWars_prog11_2019', progression: 'Practice'

stage 'Loops: Happy Loops', flex_category: 'csf_loops'
level 'HappyLoops-Unplugged'

stage 'Loops in Ice Age', flex_category: 'csf_loops'
level 'courseB_iceage_loops1_2019', progression: 'Practice'
level 'courseB_iceage_loops2_2019', progression: 'Practice'
level 'courseB_iceage_loops_video_2019', progression: 'Ice Age Loops'
level 'courseB_iceage_loops3_2019', progression: 'Practice'
level 'courseB_iceage_loops4_2019', progression: 'Practice'
level 'courseB_iceage_loops6_2019', progression: 'Practice'
level 'courseB_iceage_loops7_2019', progression: 'Practice'
level 'courseB_iceage_loops8_2019', progression: 'Practice'
level 'courseB_iceage_loops9_2019', progression: 'Practice'
level 'courseB_iceage_loops10_2019', progression: 'Challenge', challenge: true
level 'courseB_iceage_loops11_2019', progression: 'Practice'
level 'courseB_iceage_loops12_2019', progression: 'Practice'

stage 'Loops in Collector', flex_category: 'csf_loops'
level 'courseAB_video_collector_2019', progression: 'The Collector'
level 'courseA_collector_loops1_2019', progression: 'Practice'
level 'courseA_collector_loops2_2019', progression: 'Practice'
level 'courseA_video_Loops_2019', progression: 'Using the Repeat Block'
level 'courseA_collector_loops3_2019', progression: 'Practice'
level 'courseA_collector_loops4_2019', progression: 'Practice'
level 'courseA_collector_loops5_2019', progression: 'Practice'
level 'courseA_collector_loops6_2019', progression: 'Practice'
level 'courseA_collector_loops7_2019', progression: 'Practice'
level 'courseA_collector_loops8_2019', progression: 'Practice'
level 'courseA_collector_loops9_2019', progression: 'Challenge', challenge: true
level 'courseA_collector_loops10_2019', progression: 'Practice'
level 'courseA_collector_loops11_2019', progression: 'Practice'
level 'courseA_collector_loops12_2019', progression: 'Practice'
bonus 'courseA_collector_loops_challenge2kp_2019'
bonus 'courseA_collector_loops_challenge1_2019'

stage 'Ocean Scene with Loops', flex_category: 'csf_loops'
level 'courseB_video_ArtistIntro_2019', progression: 'The Artist in Code Studio'
level 'courseA_artist_loops1_2019', progression: 'Practice'
level 'courseA_artist_loops2_2019', progression: 'Practice'
level 'courseA_artist_loops3_2019', progression: 'Practice'
level 'courseA_video_loops1_2019', progression: 'Practice'
level 'courseA_artist_loops4_2019', progression: 'Practice'
level 'courseA_artist_loops5_2019', progression: 'Repeat Blocks with the Artist'
level 'courseA_artist_loops6_2019', progression: 'Practice'
level 'courseA_artist_loops7_2019', progression: 'Practice'
level 'courseA_artist_loops8_2019', progression: 'Practice'
level 'courseA_artist_loops9_2019', progression: 'Challenge', challenge: true
level 'courseA_artist_loops10_2019', progression: 'Practice'
level 'courseA_artist_loops11_2019', progression: 'Practice'
level 'courseA_artist_loops12_2019', progression: 'Free Play'
bonus 'courseA_artist_loops_challenge2a_2019'
bonus 'courseA_artist_loops_challenge1_2019'

stage 'Events: The Big Event', flex_category: 'csf_events'
level 'BigEvent-Unplugged'

stage 'Events in Play Lab', flex_category: 'csf_events'
level 'courseA_video_events_2019', progression: 'Create a Story'
level 'courseA_playLab_events1_2019', progression: 'Free Play'
level 'courseA_playLab_events2_2019', progression: 'Practice'
level 'courseA_playLab_events3_2019', progression: 'Mini-project: Jorge the Dog'
level 'courseA_playLab_events4_2019', progression: 'Mini-project: Jorge the Dog'
level 'courseA_playLab_events5_2019', progression: 'Mini-project: Jorge the Dog'
level 'courseA_playLab_events6_2019', progression: 'Mini-project: Jorge the Dog'
level 'courseA_playLab_events7_2019', progression: 'Mini-project: Jorge the Dog'
bonus 'courseA_playlab_events_challenge2_2019'
bonus 'courseA_playlab_events_challenge1_2019'
